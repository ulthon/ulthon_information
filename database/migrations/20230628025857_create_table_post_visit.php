<?php

use app\common\ColumnFormat;
use think\migration\db\Column;
use think\migration\Migrator;

class CreateTablePostVisit extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('post_visit');
        $table->addColumn(ColumnFormat::timestamp('create_time'));
        $table->addColumn(ColumnFormat::timestamp('update_time'));
        $table->addColumn(ColumnFormat::timestamp('delete_time'));
        $table->addColumn(ColumnFormat::integer('post_id'));
        $table->addColumn(ColumnFormat::stringUrl('ip'));
        $table->addColumn(ColumnFormat::stringNormal('uid'));
        $table->addColumn(ColumnFormat::stringNormal('nickname'));
        $table->addColumn(ColumnFormat::stringUrl('avatar'));
        $table->addColumn(ColumnFormat::stringNormal('client_bot'));
        $table->addColumn(Column::text('client'));
        $table->addColumn(Column::text('client_os'));
        $table->addColumn(ColumnFormat::stringNormal('client_device_name'));
        $table->addColumn(ColumnFormat::stringNormal('client_brand_name'));
        $table->addColumn(ColumnFormat::stringNormal('client_model'));
        $table->addIndex('delete_time');
        $table->addIndex('post_id');
        $table->addIndex('ip');
        $table->addIndex('uid');
        $table->create();
    }
}
