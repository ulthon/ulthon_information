<?php

use app\common\ColumnFormat;
use think\migration\Migrator;
use think\migration\db\Column;

class CreateTableTempImg extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('temp_img');

        $table->addColumn(ColumnFormat::stringUrl('path'))->setComment('临时文件路径');

        $table->addColumn(ColumnFormat::timestamp('create_time'));
        $table->addColumn(ColumnFormat::timestamp('update_time'));
        $table->addColumn(ColumnFormat::timestamp('delete_time'));

        $table->create();
    }
}
