<?php

declare(strict_types=1);

namespace app\model;

use app\common\model\Base;
use app\common\tools\Image;
use app\common\tools\PostBlock;
use app\common\tools\PostShow;
use League\HTMLToMarkdown\Converter\TableConverter;
use League\HTMLToMarkdown\HtmlConverter;
use think\facade\Cache;
use think\facade\Request;
use think\model\concern\SoftDelete;

/**
 * @mixin think\Model
 */
class Post extends Base
{
    use SoftDelete;
    //

    public const CACHE_KEY_HITS = 'cache_hits_';

    public static $autoClearCache = [

    ];

    public static $stausNameList = [
        0 => '不发布',
        1 => '发布',
    ];

    protected $defaultSoftDelete = 0;

    public function categorys()
    {
        return $this->hasMany(PostCategory::class, 'post_id');
    }

    public function tags()
    {
        return $this->hasMany(PostTag::class, 'post_id');
    }

    public function comments()
    {
        return $this->hasMany(PostComment::class, 'post_id');
    }

    public function getCommentCountAttr()
    {
        return PostComment::getPostCommentsCount($this->getData('id'));
    }

    public function setPublishTimeAttr($value)
    {
        return strtotime($value);
    }

    public function getPublishTimeTextAttr()
    {
        $value = $this->getData('publish_time');

        return date('Y-m-d', $value);
    }

    public function getPublishTimeDatetimeAttr()
    {
        $value = $this->getData('publish_time');

        return date('Y-m-d H:i:s', $value);
    }

    public function getCategorysListAttr()
    {
        $list_post_categorys = $this->getAttr('categorys');

        $list = array_column($list_post_categorys->append(['category'])->toArray(), 'category');

        $list = array2level($list, 0, 0);

        return $list;
    }

    public function getTagsListAttr()
    {
        $list_post_tags = $this->getAttr('tags');

        $list = array_column($list_post_tags->append(['tag'])->toArray(), 'tag');

        return $list;
    }

    public function getDescAttr($value)
    {
        if (empty($value)) {
        }

        return $value;
    }

    public function getDescShortAttr()
    {
        $desc = $this->getData('desc');

        if (strlen($desc) > 100) {
            $desc = mb_substr($desc, 0, 100) . '...';
        }

        return $desc;
    }

    public function getDescListAttr()
    {
        $desc = $this->getData('desc');

        if (empty($desc)) {
            return '';
        }
        $list = explode("\n", $desc);

        return $list;
    }

    public function getDescHtmlAttr()
    {
        $desc = $this->getData('desc');

        if (empty($desc)) {
            return '';
        }

        return str_replace("\n", '<br>', $desc);
    }

    public function getStatusNameAttr()
    {
        return self::$stausNameList[$this->getData('status')];
    }

    public function setPubishTimeAttr($value)
    {
        return strtotime($value);
    }

    public function setContentHtmlAttr($value)
    {
        return trim($value);
    }

    public function getContentMarkdownAttr()
    {
        $content_html = $this->getAttr('content_html');

        $content_html .= PostBlock::copyright($this);

        $converter = new HtmlConverter(['strip_tags' => true]);

        $converter->getEnvironment()->addConverter(new TableConverter());

        $markdown = $converter->convert($content_html);

        return $markdown;
    }

    public function getContentHtmlShowAttr()
    {
        $content = $this->getAttr('content_html');

        $content = PostShow::handleImage($content);

        return PostShow::handleCopyright($content);
    }

    public function getPosterAttr($value)
    {
        if (empty($value)) {
            $value = '/static/images/avatar.png';
        }

        return get_source_link($value);
    }

    public function getPosterShowAttr()
    {
        $poster = $this->getAttr('poster');

        $watermarked_file_save_name = Image::handelWatermarkSave($poster);

        return $watermarked_file_save_name;
    }

    public function getReadUrlAttr()
    {
        $path = '/index/a' . $this->getData('uid') . '.html';

        return Request::domain() . $path;
    }

    public function getShareTextAttr()
    {
        $share_text = get_system_config('post_share_text_tpl');

        $share_text = str_replace('%post_title%', $this->getAttr('title'), $share_text);
        $share_text = str_replace('%post_desc%', $this->getAttr('desc'), $share_text);
        $share_text = str_replace('%post_url%', $this->getAttr('read_url'), $share_text);

        return $share_text;
    }

    public function getStartContentAttr()
    {
        $start_content = '';

        $list_post_category_id = $this->getAttr('categorys')->column('category_id');

        $list_start_post = self::where('category_id', 'in', $list_post_category_id)->where('type', 'category-start')->cacheAlways(600)->where('status', 1)->select();

        foreach ($list_start_post as $k_start_post => $v_start_post) {
            $start_content .= $v_start_post->content_html;
        }

        return $start_content;
    }

    public function getEndContentAttr()
    {
        $end_content = '';
        $list_post_category_id = $this->getAttr('categorys')->column('category_id');
        $list_end_post = self::where('category_id', 'in', $list_post_category_id)->where('type', 'category-end')->cacheAlways(600)->where('status', 1)->select();
        foreach ($list_end_post as $k_end_post => $v_end_post) {
            $end_content .= $v_end_post->content_html;
        }

        return $end_content;
    }

    public function getHitsTitleAttr()
    {
        $cache_key = static::CACHE_KEY_HITS . $this->getAttr('id');

        $value = Cache::get($cache_key);

        if (!is_null($value)) {
            return $value;
        }

        $value = $this->getData('hits');

        Cache::set($cache_key, $value, 600);

        return $value;
    }

    public function setHitsAttr($value)
    {
        $cache_key = static::CACHE_KEY_HITS . $this->getAttr('id');

        $this->getAttr('hits_title');
        Cache::inc($cache_key);

        return $value;
    }
}
