<?php

declare(strict_types=1);

namespace app\model;

use app\common\model\Base;
use app\common\tools\Image;

/**
 * @mixin think\Model
 */
class Nav extends Base
{
    public static $autoClearCache = [
        [
            'type' => 'key',
            'name' => 'type_list',
            'field' => 'type',
        ],
        [
            'type' => 'tag',
            'name' => 'page_cache',
        ],
    ];

    public static $statusName = [
        0 => '不显示',
        1 => '显示',
    ];

    //
    public function getImgAttr($value)
    {
        return get_source_link($value);
    }

    public function getImgShowAttr()
    {
        $src = $this->getData('img');

        $watermarked_file_save_name = Image::handelWatermarkSave($src);

        return $watermarked_file_save_name;
    }

    public function getStatusNameAttr()
    {
        return self::$statusName[$this->getData('status')];
    }
}
