<?php

declare(strict_types=1);

namespace app\model;

use think\facade\Request;
use think\Model;

/**
 * @mixin \think\Model
 */
class PostVisit extends Model
{
    //

    protected $json = [
        'client_bot',
        'client_os',
        'client',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function getAvatarSrcAttr()
    {
        $value = $this->getAttr('avatar');

        if (empty($value)) {
            $value = '/static/images/avatar.png';
        }

        return $value;
    }

    public function getNicknameTitleAttr()
    {
        $value = $this->getAttr('nickname');

        if (empty($value)) {
            $current_ip = Request::ip();

            $ip = $this->getAttr('ip_title');

            if ($current_ip == $ip) {
                $value = '您的IP:' . $ip;
            } else {
                $value = 'IP用户:' . $ip;
            }
        }

        return $value;
    }

    public function getIpTitleAttr()
    {
        $value = $this->getAttr('ip');

        if (empty($value)) {
            return $value;
        }

        $ip_pieces = explode('.', $value);

        $ip_pieces = array_slice($ip_pieces, 0, -2);

        $ip_pieces[] = '*';
        $ip_pieces[] = '*';

        return implode('.', $ip_pieces);
    }

    public function getCreateTimeTitleAttr()
    {
        $value = $this->getData('create_time');

        return show_time_ago($value);
    }

    public function getClientNameAttr()
    {
        $client = $this->getAttr('client');

        return $client->name ?? '';
    }

    public function getClientOsNameAttr()
    {
        $client_os = $this->getAttr('client_os');

        $name = $client_os->name ?? '';
        $version = $client_os->version ?? '';

        return "$name $version";
    }

    public function getClientBotNameAttr() 
    {
        $client_bot = $this->getAttr('client_bot');

        return $client_bot->name ?? '';
    }
}
