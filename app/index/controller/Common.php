<?php

namespace app\index\controller;

use app\model\Category;
use app\model\Nav;
use app\model\Post;
use app\model\PostVisit;
use think\facade\Cache;
use think\facade\Session;
use think\facade\View;
use UserHub\Client;

class Common extends BaseController
{
    protected $userinfo = [];

    public function initialize()
    {
        parent::initialize();

        $site_logo = get_source_link(get_system_config('site_logo'));

        View::assign('site_logo', $site_logo);
        $site_logo_box = get_source_link(get_system_config('site_logo_box'));

        View::assign('site_logo_box', $site_logo_box);

        $list_nav_slide = Nav::where('type', 3)->cacheAlways('type_list_3')->order('sort asc')->where('status', 1)->select();

        View::assign('list_nav_slide', $list_nav_slide);

        $list_nav_friend_url = Nav::where('type', 2)->cacheAlways('type_list_2')->order('sort asc')->where('status', 1)->select();
        View::assign('list_nav_friend_url', $list_nav_friend_url);

        $list_header_nav = Nav::where('type', 11)->cacheAlways('type_list_11')->order('sort asc')->where('status', 1)->select();
        View::assign('list_header_nav', $list_header_nav);

        $list_category_first_level = Category::where('level', 1)->where('status', 1)->where('type', 3)->cacheAlways('category_type_list_3')->order('sort asc')->select();
        View::assign('list_category_first_level', $list_category_first_level);
        $list_nav_more = Nav::where('type', 8)->cacheAlways('type_list_8')->order('sort asc')->where('status', 1)->select();
        View::assign('list_nav_more', $list_nav_more);

        $top_posts = Post::where('is_top', 1)->limit(8)->where('type', 3)->cacheAlways(60)->select();
        View::assign('top_posts', $top_posts);

        $list_site_last_visit = PostVisit::with(['post'])->order('id desc')
        ->group('post_id,ip,uid')
        ->limit(6)
        ->cacheAlways(60)
        ->select();
        View::assign('list_site_last_visit', $list_site_last_visit);

        $total_hits = Post::cacheAlways(60)->sum('hits');
        View::assign('total_hits', $total_hits);

        $total_week_hits = PostVisit::cacheAlways(60)->where('create_time', '>', strtotime(date('Y-m-d 00:00:00')) - 86400 * 7)->count();
        $total_day_hits = PostVisit::cacheAlways(60)->where('create_time', '>', strtotime(date('Y-m-d 00:00:00')))->count();
        View::assign('total_week_hits', $total_week_hits);
        View::assign('total_day_hits', $total_day_hits);

        $total_post_count = Post::cacheAlways(60)->where('status', 1)->where('type', 3)->count();
        View::assign('total_post_count', $total_post_count);

        $total_month_post_count = Post::cacheAlways(60)->where('status', 1)->where('type', 3)->where('publish_time', '>', strtotime(date('Y-m-1 00:00:00')))->count();
        View::assign('total_month_post_count', $total_month_post_count);

        $this->userHubLogin();
    }

    public function userHubLogin()
    {
        $user_uid = Session::get('user_uid');
        $user_info = [];
        if (empty($user_uid)) {
            $code = $this->request->param('code');

            // 实例化客户端,传入相关参数
            $user_hub_client = new Client([
                'key' => get_system_config('user_hub_key'),
                'secret' => get_system_config('user_hub_secret'),
                'host' => get_system_config('user_hub_host'),
            ]);
            if (empty($code)) {
                // 跳转登录
                $url = $user_hub_client->getBowserRedirectUrl($this->request->url(true));
                View::assign('login_url', $url);
            } else {
                // 获取用户信息
                $user_info = $user_hub_client->getUserinfoByCode($code);
                Session::set('user_uid', $user_info['uid']);
            }
        } else {
            $user_info = self::getUserInfo($user_uid);
        }
        $this->userinfo = $user_info;
        View::assign('user_info', $user_info);
    }

    public static function getUserInfo($uid)
    {
        $cache_key = 'user_uid_' . $uid;
        $user_info = Cache::get($cache_key);

        if (empty($user_info)) {
            // 实例化客户端,传入相关参数
            $user_hub_client = new Client([
                'key' => get_system_config('user_hub_key'),
                'secret' => get_system_config('user_hub_secret'),
                'host' => get_system_config('user_hub_host'),
            ]);

            $user_info = $user_hub_client->getUserinfoByUid($uid);

            Cache::set($cache_key, $user_info);
        }

        return $user_info;
    }
}
