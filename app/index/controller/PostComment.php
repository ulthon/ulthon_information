<?php

declare(strict_types=1);

namespace app\index\controller;

use app\model\Post;
use app\model\PostComment as ModelPostComment;
use think\facade\Cache;
use think\facade\Session;
use think\facade\Validate;
use think\Request;
use think\validate\ValidateRule;

class PostComment extends Common
{
    /**
     * 保存新建的资源.
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //

        $user_uid = Session::get('user_uid');

        if (empty($user_uid)) {
            return json_message('请先登录');
        }

        $post_data = $request->post();

        $validate = Validate::rule('post_id', ValidateRule::isRequire())
            ->rule('content|评论内容', ValidateRule::isRequire()->length('5,200', '评论字符长度需要在5-200之间'));

        if (!$validate->check($post_data)) {
            return json_message($validate->getError());
        }

        $model_post = Post::find($post_data['post_id']);

        if (empty($model_post)) {
            return json_message('文章不存在');
        }

        $post_data['type'] = 3;

        $post_data['user_uid'] = $user_uid;

        ModelPostComment::create($post_data);

        ModelPostComment::clearCountCache();

        Cache::delete('post_' . $model_post->uid);

        return json_message();
    }

    /**
     * 删除指定资源.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //

        ModelPostComment::destroy($id);

        ModelPostComment::clearCountCache();

        return json_message();
    }
}
