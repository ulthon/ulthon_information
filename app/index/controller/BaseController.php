<?php

namespace app\index\controller;

use app\BaseController as AppBaseController;
use app\common\Bootstrap;
use think\exception\HttpResponseException;
use think\facade\Env;
use think\facade\Request;
use think\Paginator;

class BaseController extends AppBaseController
{
    public function initialize()
    {
        parent::initialize();

        Paginator::maker(function ($items, $listRows, $currentPage, $total, $simple, $options) {
            return new Bootstrap($items, $listRows, $currentPage, $total, $simple, $options);
        });

        $debug = Env::get('app_debug');

        if (!$debug) {
            // 调试模式不要跳转域名和协议
            $main_domain = get_system_config('main_domain');
            $url = Request::url();

            if (!empty($main_domain)) {
                if (Request::host() != $main_domain) {
                    throw new HttpResponseException(redirect(Request::scheme() . '://' . $main_domain . '' . $url));
                }
            }

            $is_jump_https = get_system_config('is_jump_https', 0);

            if ($is_jump_https == 1) {
                if (Request::scheme() != 'https') {
                    throw new HttpResponseException(redirect('https://' . Request::host() . '' . $url));
                }
            }
        }
    }
}
