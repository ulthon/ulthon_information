<?php

namespace app\index\controller;

use app\model\Category;
use app\model\Post;
use app\model\PostCategory;
use app\model\PostVisit;
use think\facade\Cache;
use think\facade\Session;
use think\facade\View;

class Index extends Common
{
    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $page_cache_key = md5($this->request->url());

        $content = Cache::get($page_cache_key);

        if (!env('app_debug') && !empty($content)) {
            return $content;
        }

        $page_title = '全部文章';

        //
        $sub_category = [];
        $current_category = [];

        $category_id = $this->request->param('category_id');

        if (!empty($category_id)) {
            $current_category = Category::find($category_id);

            $page_title = $current_category->title;

            $sub_category = Category::where('pid', $category_id)->where('type', 3)->order('sort asc')->select();

            $sub_category_id = $this->request->param('sub_category_id');
            if (empty($sub_category_id)) {
                $categorys = [$category_id];

                $categorys = array_merge($categorys, array_column((array) Category::getListLevel($category_id), 3));

                $categorys_where = PostCategory::whereIn('category_id', $categorys);

                $model_post = Post::hasWhere('categorys', $categorys_where)->where('status', 1)->order('publish_time desc');
            } else {
                $model_sub_category = Category::find($sub_category_id);

                $page_title .= "-{$model_sub_category->title}";
                $model_post = Post::hasWhere('categorys', ['category_id' => $sub_category_id])->where('status', 1)->order('publish_time desc');
            }
        } else {
            $model_post = Post::where('status', 1)->order('publish_time desc');
        }

        $model_post->where('type', 3);

        $keywords = $this->request->param('keywords');

        if (!empty($keywords)) {
            $model_post->whereLike('title|desc|content', "%$keywords%");

            $page_title .= "-{$keywords}";
        }

        $page = $this->request->param('page', 1);

        $page_title .= "-第{$page}页";

        $list_post = $model_post->paginate([
            'url' => 'Index/index',
            'query' => [
                'category_id' => $this->request->param('category_id'),
                'sub_category_id' => $this->request->param('sub_category_id'),
                'page' => $page,
                'keywords' => $keywords,
            ],
            'list_rows' => 10,
        ]);

        View::assign('current_category', $current_category);
        View::assign('sub_category', $sub_category);

        View::assign('list_post', $list_post);
        View::assign('page_title', $page_title);

        $content = View::fetch();

        Cache::tag('page_cache')->set($page_cache_key, $content, 600);

        return $content;
    }

    public function visit()
    {
        $post_id = $this->request->param('post_id');

        $model_list_visit = PostVisit::with(['post'])->order('id desc');

        if(!empty($post_id)) {
            $model_list_visit->where('post_id', $post_id);
        }
        $model_list_visit->cache(60);
        $list_visit = $model_list_visit->paginate([
            'url' => 'Index/visit',
            'list_rows' => 20,
            'query' => [
                'post_id' => $post_id,
            ],
        ]);
        View::assign('list_visit', $list_visit);

        return View::fetch();
    }

    public function logout()
    {
        Session::clear();

        $back_url = $this->request->param('back_url', '/');

        return $this->success('退出成功', $back_url);
    }
}
