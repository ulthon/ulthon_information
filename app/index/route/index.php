<?php

use think\facade\Route;

Route::rule('a:uid', 'Post/read');
Route::rule('i<category_id?>/s<sub_category_id?>/p<page?>', 'Index/index');
Route::rule('i<category_id?>/s/p<page?>', 'Index/index');
Route::rule('i/s/p<page?>', 'Index/index');

