<?php
namespace app\common\model;

use app\common\traits\AutoClearCache;
use think\Model;

class Base extends Model 
{
    use AutoClearCache;
}
