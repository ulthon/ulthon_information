<?php

namespace app\common;

use think\paginator\driver\Bootstrap as DriverBootstrap;

class Bootstrap extends DriverBootstrap
{

  /**
   * 获取页码对应的链接
   *
   * @access protected
   * @param int $page
   * @return string
   */
  protected function url(int $page): string
  {
    $parameters = ['page' => $page];
    if (count($this->options['query']) > 0) {
      $parameters = array_merge($this->options['query'], ['page' => $page]);
    }
    return (string)url($this->options['url'], $parameters);
  }
}
