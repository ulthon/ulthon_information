<?php

namespace app\common\tools;


use DateTime;
use Thepixeldeveloper\Sitemap\Drivers\XmlWriterDriver;
use Thepixeldeveloper\Sitemap\Url;
use Thepixeldeveloper\Sitemap\Urlset;
use think\facade\Request;

class Sitemap
{
    public static function init()
    {

        // $urlset_page_list = new Urlset();

        $urlset_page_post = new Urlset();

        $index_url = new Url(Site::indexUrl());

        $index_url->setChangeFreq('always');

        $index_url->setPriority(0.9);

        $urlset_page_post->add($index_url);

        $list_post = Site::mapAllPost();

        foreach ($list_post as $model_post) {
            $url_post = new Url($model_post->read_url);
            $url_post->setChangeFreq('yearly');

            $last_mod_date = new DateTime($model_post->publish_time_datetime);

            $url_post->setLastMod($last_mod_date);

            $url_post->setPriority(0.9);

            $urlset_page_post->add($url_post);
        }


        $dirver = new XmlWriterDriver();
        // $urlset_page_list->accept($dirver);

        $urlset_page_post->accept($dirver);

        return $dirver->output();
    }
}
