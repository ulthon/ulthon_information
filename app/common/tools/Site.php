<?php
namespace app\common\tools;

use think\facade\Request;

class Site
{
    public static function mapAllPost()
    {
        $list_post = \app\model\Post::cache(60)->where('type',3)->where('status', 1)->select();

        return $list_post;
    }
    public static function mapRecentlyPost()
    {
        $list_post = \app\model\Post::cache(60)->where('type',3)->where('status', 1)->order('publish_time', "desc")->limit(25)->select();

        return $list_post;
    }

    public static function name()
    {
        return get_system_config('site_name');
    }
    public static function author()
    {
        return get_system_config('default_author');
    }

    public static function indexUrl()
    {
        return Request::scheme() . '://' . get_system_config('main_domain', Request::host());
    }

    public static function desc()
    {
        return get_system_config('site_desc');
    }

    public static function logo()
    {
        return get_source_link(get_system_config('site_logo'));
    }

    public static function keywords()
    {
        return get_system_config('site_keywords');
    }

    public static function sitemapUrl()
    {
        return Request::scheme() . '://' . get_system_config('main_domain', Request::host()) . '/sitemap.xml';
    }
    public static function rss1Url()
    {
        return Request::scheme() . '://' . get_system_config('main_domain', Request::host()) . '/rss1.xml';
    }
    public static function rss2Url()
    {
        return Request::scheme() . '://' . get_system_config('main_domain', Request::host()) . '/rss2.xml';
    }
    public static function atomUrl()
    {
        return Request::scheme() . '://' . get_system_config('main_domain', Request::host()) . '/atom.xml';
    }
}