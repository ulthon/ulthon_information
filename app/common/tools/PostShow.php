<?php

namespace app\common\tools;

use phpQuery;

class PostShow
{
    public static function handleCopyright($content)
    {
        // 防采集版权声明,被采集了会在其他网站显示
        // 可以被选中复制，可以被采集，在网页中不会显示，但是在别的网站中可以显示
        $content_copyright = '<blockquote class="post-content-inner-copyright"> 版权声明：' . get_system_config('post_copyright') . '</blockquote>';

        // 防采集版权声明，被采集了也不会在其他网站显示
        // 可以被选中复制，可以被采集，在网页中不会显示，在别的网站中也不会显示
        $content_copyright_hide = '<p style="height:0;line-height: 0;margin:0;padding:0;overflow: hidden;margin-block-start:0;margin-block-end:0;position: absolute;">版权声明：' . get_system_config('post_copyright') . '</p>';

        $dom_copyright = phpQuery::newDocumentHTML($content_copyright);

        $dom_copyright_hide = phpQuery::newDocumentHTML($content_copyright_hide);

        $dom_content = phpQuery::newDocumentHTML($content);

        // 找到子元素p
        $p_list = $dom_content->children('p');

        foreach ($p_list as $index_p => $dom_p) {
            if ($index_p != 0 && $index_p % 8 == 0) {
                // $dom_copyright->clone()->children()->insertAfter($dom_p);
            }
            if ($index_p != 0 && $index_p % 6 == 0) {
                $dom_copyright_hide->clone()->children()->insertAfter($dom_p);
            }
        }

        $content = $dom_content->html();

        return $content;
    }

    public static function handleImage($content)
    {
        $dom_content = phpQuery::newDocumentHTML($content);

        $img_list = $dom_content->find('img');

        foreach ($img_list as $index_img => $dom_img) {
            $dom_img = pq($dom_img);

            $src = $dom_img->attr('src');

            if (strpos($src, 'data:image') !== 0) {
                $src = get_source_link($src);
            }

            $watermarked_file_save_name = Image::handelWatermarkSave($src);

            $dom_img->attr('src', $watermarked_file_save_name);
        }

        $content = $dom_content->html();

        return $content;
    }
}
