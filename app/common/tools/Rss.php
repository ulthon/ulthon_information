<?php
namespace app\common\tools;

use FeedWriter\ATOM;
use FeedWriter\RSS1;
use FeedWriter\RSS2;

class Rss
{
    public static function initRss1()
    {
        $feed = new RSS1();


        $feed->setTitle(Site::name());
        $feed->setLink(Site::indexUrl());
        $feed->setDescription(Site::desc());

        //It's important for RSS 1.0
        $feed->setChannelAbout(Site::keywords());

        // An image is optional.
        $feed->setImage(Site::logo(), Site::name() . '头像', get_source_link('/'));

        //Adding a feed. Generally this portion will be in a loop and add all feeds.

        $list_post = Site::mapRecentlyPost();

        foreach ($list_post as $model_post) {
            //Create an empty FeedItem

            $new_item = $feed->createNewItem();

            $new_item->setTitle($model_post->title);

            $new_item->setLink($model_post->read_url);
            //The parameter is a timestamp for setDate() function
            $new_item->setDate($model_post->publish_time);
            $new_item->setDescription($model_post->desc);
            //Use core addElement() function for other supported optional elements

            //Now add the feed item
            $feed->addItem($new_item);
        }

        $content = $feed->generateFeed();

        return $content;
    }

    public static function initRss2()
    {

        // Creating an instance of RSS2 class.
        $feed = new RSS2();

        // Setting some basic channel elements. These three elements are mandatory.
        $feed->setTitle(Site::name());

        $feed->setLink(Site::indexUrl());

        $feed->setDescription(Site::desc());


        // Image title and link must match with the 'title' and 'link' channel elements for RSS 2.0,
// which were set above.
        $feed->setImage(Site::logo(), Site::name() . '头像', Site::indexUrl());

        // Use the setChannelElement() function for other optional channel elements.
// See http://www.rssboard.org/rss-specification#optionalChannelElements
// for other optional channel elements. Here the language code for American English is used for the
// optional "language" channel element.
        $feed->setChannelElement('language', 'zh-cn');

        $list_post = Site::mapRecentlyPost();

        // The date when this feed was lastly updated. The publication date is also set.
        $feed->setDate($list_post[0]->publish_time);

        $feed->setChannelElement('pubDate', $list_post[0]->publish_time);

        $feed->setSelfLink(Site::rss2Url());
        // $feed->setAtomLink('http://pubsubhubbub.appspot.com', 'hub');

        // If you want you can also add a line to publicly announce that you used
// this fine piece of software to generate the feed. ;-)
        $feed->addGenerator();

        // Here we are done setting up the feed. What's next is adding some feed items.

        foreach ($list_post as $model_post) {
            # code...
            $new_item = $feed->createNewItem();

            // Add basic elements to the feed item
            // These are again mandatory for a valid feed.
            $new_item->setTitle($model_post->title);

            $new_item->setLink($model_post->read_url);

            $new_item->setDescription($model_post->desc);


            // The following method calls add some optional elements to the feed item.

            // Let's set the publication date of this item. You could also use a UNIX timestamp or
            // an instance of PHP's DateTime class.
            $new_item->setDate($model_post->publish_time);

            // If you want you can set the name (and email address) of the author of this feed item.
            $new_item->setAuthor($model_post->author_name, get_system_config('stie_contact_email'));

            // You can set a globally unique identifier. This can be a URL or any other string.
            // If you set permaLink to true, the identifier must be an URL. The default of the
            // permaLink parameter is false.
            $new_item->setId($model_post->read_url, true);

            // Now add the feed item to the main feed.
            $feed->addItem($new_item);
        }


        // OK. Everything is done. Now generate the feed.
        // Then do anything (e,g cache, save, attach, print) you want with the feed in $myFeed.
        $content = $feed->generateFeed();

        return $content;
    }
    public static function initAtom()
    {
        $feed = new ATOM();

        //Setting the channel elements
//Use wrapper functions for common elements
        $feed->setTitle(Site::name());
        $feed->setDescription(Site::desc());

        $feed->setLink(Site::indexUrl());

        $list_post = Site::mapRecentlyPost();

        $feed->setDate($list_post[0]->publish_time);
        $feed->setImage(Site::logo());

        //For other channel elements, use setChannelElement() function
        $feed->setChannelElement('author', array('name' => Site::author()));


        //You can add additional link elements, e.g. to a PubSubHubbub server with custom relations.
        $feed->setSelfLink(Site::atomUrl());
        // $feed->setAtomLink('http://pubsubhubbub.appspot.com', 'hub');

        //Adding a feed. Generally this portion will be in a loop and add all feeds.

        foreach ($list_post as $model_post) {

            $new_item = $feed->createNewItem();

            //Add elements to the feed item
            //Use wrapper functions to add common feed elements
            $new_item->setTitle($model_post->title);

            $new_item->setLink($model_post->read_url);

            $new_item->setDate($model_post->publish_time);

            $new_item->setAuthor($model_post->author_name, get_system_config('stie_contact_email'));


            //Internally changed to "summary" tag for ATOM feed
            $new_item->setDescription($model_post->desc);
            // $new_item->setContent($model_post->content_html);

            //Now add the feed item
            $feed->addItem($new_item);

        }

        $content = $feed->generateFeed();
        return $content;
    }
}