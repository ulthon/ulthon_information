<?php

namespace app\common\tools;

use think\facade\View;

class PostBlock
{
    public static function  copyright($model_post)
    {

        return View::fetch(__DIR__ . '/post_block/copyright.html', [
            'post' => $model_post
        ]);
    }
}
