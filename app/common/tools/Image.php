<?php

namespace app\common\tools;

use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Exception\NotSupportedException;
use Intervention\Image\Image as ImageImage;
use Intervention\Image\ImageManagerStatic;
use SplFileInfo;
use think\facade\App;

class Image
{
    public static function addWatermark(SplFileInfo $file, $watermark_str = 'phpreturn.com'):ImageImage|bool
    {
        ImageManagerStatic::configure(['driver' => 'imagick']);

        try {
            $img = ImageManagerStatic::make($file->getPathname());
        } catch (NotReadableException|NotSupportedException $th) {
            return false;
        }

        $font_size = 20;

        // 字体大小为宽度的 1/20
        $font_size = $img->width() / 40;
        $font_size = ceil($font_size);

        $x = $img->width() - $font_size;
        $y = $img->height() - $font_size;

        $img->text($watermark_str, $x + $font_size * 0.05, $y + $font_size * 0.05, function ($font) use ($font_size) {
            $font->file(App::getRootPath() . '/source/font/SourceHanSans-Normal.otf');
            $font->size($font_size);
            $font->color([0, 0, 0, 0.9]);
            $font->align('right');
            $font->valign('bottom');
        });
        $img->text($watermark_str, $x, $y, function ($font) use ($font_size) {
            $font->file(App::getRootPath() . '/source/font/SourceHanSans-Normal.otf');
            $font->size($font_size);
            $font->color([255, 255, 255, 0.9]);
            $font->align('right');
            $font->valign('bottom');
        });

        return $img;
    }

    public static function handelWatermarkSave($src, $watermarked_file_save_name = null)
    {
        // 判断src是否是base64
        if (strpos($src, 'data:image') === 0) {
            $src = static::base64ToImage($src);
        }

        $src_file = new SplFileInfo($src);
        $watermark_text = get_system_config('watermark_text', 'phpreturn.com');

        $src_md5 = md5($src . $watermark_text);

        if (!$watermarked_file_save_name) {
            $watermarked_file_save_name = '/upload/watermark/' . $src_md5 . '.' . $src_file->getExtension();
        }

        // 如果以jpe结尾，转换为jpg
        if (str_ends_with($watermarked_file_save_name, '.jpe')) {
            $watermarked_file_save_name = str_replace('.jpe', '.jpg', $watermarked_file_save_name);
        }

        $watermarked_file = App::getRootPath() . '/public' . $watermarked_file_save_name;

        if (file_exists($watermarked_file)) {
            return $watermarked_file_save_name;
        }

        if (!is_dir(dirname($watermarked_file))) {
            mkdir(dirname($watermarked_file), 0777, true);
        }

        $img = static::addWatermark($src_file, $watermark_text);

        if ($img) {
            $img->save($watermarked_file);
        } else {
            // 把图片复制到新的位置
            copy($src, $watermarked_file);
        }

        return $watermarked_file_save_name;
    }

    public static function base64ToImage($base64)
    {
        $ext_name = 'jpg';

        if (strpos($base64, 'data:image/jpeg;base64,') === 0) {
            $ext_name = 'jpg';
        } elseif (strpos($base64, 'data:image/jpg;base64,') === 0) {
            $ext_name = 'jpg';
        } elseif (strpos($base64, 'data:image/png;base64,') === 0) {
            $ext_name = 'png';
        } elseif (strpos($base64, 'data:image/gif;base64,') === 0) {
            $ext_name = 'gif';
        }

        $base64 = str_replace('data:image/jpeg;base64,', '', $base64);
        $base64 = str_replace('data:image/jpg;base64,', '', $base64);
        $base64 = str_replace('data:image/png;base64,', '', $base64);
        $base64 = str_replace('data:image/gif;base64,', '', $base64);
        $base64 = str_replace(' ', '+', $base64);

        $image = base64_decode($base64);

        $file_name = '/upload/base64/' . md5($base64) . '.' . $ext_name;
        $file_path = App::getRootPath() . '/public' . $file_name;

        if (!is_dir(dirname($file_path))) {
            mkdir(dirname($file_path), 0777, true);
        }

        file_put_contents($file_path, $image);

        return $file_path;
    }
}
