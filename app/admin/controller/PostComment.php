<?php

declare(strict_types=1);

namespace app\admin\controller;

use app\model\PostComment as ModelPostComment;
use think\facade\Cache;
use think\facade\View;
use think\Request;

class PostComment extends Common
{
  /**
   * 显示资源列表
   *
   * @return \think\Response
   */
  public function index()
  {
    //

    $type = $this->request->param('type', 1);
    $list = ModelPostComment::order('id desc')
      ->where('type', $type)
      ->paginate();

    View::assign('list',$list);

    return View::fetch();
  }

  /**
   * 显示创建资源表单页.
   *
   * @return \think\Response
   */
  public function create()
  {
    //
  }

  /**
   * 保存新建的资源
   *
   * @param  \think\Request  $request
   * @return \think\Response
   */
  public function save(Request $request)
  {
    //
  }

  /**
   * 显示指定的资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function read($id)
  {
    //
  }

  /**
   * 显示编辑资源表单页.
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * 保存更新的资源
   *
   * @param  \think\Request  $request
   * @param  int  $id
   * @return \think\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * 删除指定资源
   *
   * @param  int  $id
   * @return \think\Response
   */
  public function delete($id)
  {
    //

    ModelPostComment::destroy($id);

    ModelPostComment::clearCountCache();

    return json_message();
  }
}
