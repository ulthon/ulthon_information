<?php

namespace app\admin\controller;

use app\model\TempImg;
use app\model\UploadFiles;
use app\UploadFiles as AppUploadFiles;
use League\Flysystem\Util\MimeType;
use League\MimeTypeDetection\FinfoMimeTypeDetector;
use League\MimeTypeDetection\GeneratedExtensionToMimeTypeMap;
use League\MimeTypeDetection\MimeTypeDetector;
use think\facade\App;
use think\facade\View;
use think\Request;

class File extends Common
{
    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        //

        $type = $this->request->param('type', 1);
        $status = $this->request->param('status', '');

        $model_list = UploadFiles::withTrashed()->where('type', $type)->order('id desc');

        if ($status != '') {
            $model_list->where('status', $status);
        }

        $list = $model_list->paginate([
            'query' => $this->request->get(),
        ]);
        View::assign('list', $list);
        View::assign('type', $type);

        return View::fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源.
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //

        return AppUploadFiles::save($request);
    }

    public function editormdSave(Request $request)
    {
        return AppUploadFiles::editormdSave($request);
    }

    public function wangEditorSave(Request $request)
    {
        return AppUploadFiles::wangEditorSave($request);
    }

    public function urlSave(Request $request)
    {
        return AppUploadFiles::saveUrlFile($request->param('url'), $request->param('type'));
    }

    public function base64Save(Request $request)
    {
        return AppUploadFiles::saveBase64File($request->param('data'), $request->param('type'));
    }

    public function tempBase64Save(Request $request)
    {
        $file_data = $request->param('data');
        if (strstr($file_data, ',')) {
            $file_data = explode(',', $file_data);
            $file_data = $file_data[1];
        }
        $file_data = base64_decode($file_data);

        $mime_detector = new FinfoMimeTypeDetector();

        $mime_type = $mime_detector->detectMimeTypeFromBuffer($file_data);
        $ext_name = array_search($mime_type, GeneratedExtensionToMimeTypeMap::MIME_TYPES_FOR_EXTENSIONS);

        $public_file_path = '/temp_img/' . uniqid() . '.' . $ext_name;

        $temp_file = App::getRootPath() . 'public' . $public_file_path;

        $dirname = dirname($temp_file);

        if (!is_dir($dirname)) {
            mkdir($dirname, 0777, true);
        }

        file_put_contents($temp_file, $file_data);

        $model_img = TempImg::create([
            'path' => $public_file_path,
        ]);

        return json_message([
            'src' => $public_file_path,
        ]);
    }

    public function clearTempImg()
    {
        $list_img = TempImg::where('create_time', '<', time() - 600)->limit(100)->select();

        foreach ($list_img as $key => $model_img) {
            $temp_file = App::getRootPath() . 'public' . $model_img->path;

            if (file_exists($temp_file)) {
                unlink($temp_file);
                $model_img->delete();
            }
        }

        return json_message([
            'delete' => $list_img->count(),
        ]);
    }

    public function clear($id)
    {
        AppUploadFiles::clear($id);

        return json_message();
    }
}
