# ulthon_information

#### 介绍
奥宏官网后台

#### 软件架构
基于奥宏TP6后台管理模板开发的官网网站.

[奥宏后台管理模板系统](https://gitee.com/ulthon/ulthon_admin)

最低支持php7.1

#### 演示网址

[在线演示网址](http://phpreturn.com/)

#### 快速试用教程

##### 安装好PHP环境

安装php7.1+,

安装composer

不用必须安装nginx,mysql等

配置好PATH目录,能够直接运行PHP

[教程参考](http://baidu.apphb.com/?q=php%20%E8%AE%BE%E7%BD%AEpath%E7%8E%AF%E5%A2%83%20windows)

![ahw6Og.png](https://s1.ax1x.com/2020/08/07/ahw6Og.png)

> 建议安装gitbash [教程参考](http://baidu.apphb.com/?q=windows+%20%E5%AE%89%E8%A3%85gitbash)

##### 下载代码包

试用git或者下载压缩包,然后进入项目文件夹.

![ah0QBQ.png](https://s1.ax1x.com/2020/08/07/ah0QBQ.png)


#### 快速试用

    1.安装
    git clone https://gitee.com/ulthon/ulthon_information.git
    2.进入目录
    cd ulthon_information/
    3.安装依赖
    composer install
    4.初始化数据库
    php think migrate:run
    php think seed:run
    5.使用内置服务器
    php think run -p 8010
    6.访问前台
    127.0.0.1:8010/index.php/index
    7.访问后台
    127.0.0.1:8010/index.php/admin

后台帐号/密码：admin/123456

[本地测试后台链接](/index.php/admin)

如果希望去掉index.php，可以参考tp文档，在nginx或apache环境配置，内置服务器必须带index.php

#### 域名绑定

项目基于TP6开发,设置了域名绑定,[参考文档](https://www.kancloud.cn/manual/thinkphp6_0/1297876#_242),访问规则如下:

```
www.域名->访问前台应用

admin.域名->访问后台应用

api.域名->访问接口应用
```

#### 用户体系

本站实现了用户中心的授权登陆,如果需要登陆的话,需要额外搭建一个用户中心站点,具体请看:https://gitee.com/ulthon/user_hub


#### 重置密码

重置密码为123456.

```
php think reset_password
```

### 开源协议

木兰宽松许可证, 第2版

### 联系我

QQ群:570927792