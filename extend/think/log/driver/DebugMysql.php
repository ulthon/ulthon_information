<?php

namespace think\log\driver;

use think\contract\LogHandlerInterface;
use think\facade\App;
use PDO;

class DebugMysql  implements LogHandlerInterface
{

  protected $enableLog = true;

  protected $config = [];

  protected $pdo = null;

  protected $tableName = '';

  public function __construct(App $app, $config = [])
  {
    if (is_array($config)) {
      $this->config = array_merge($this->config, $config);
    }

    $dsn = $this->parseDsn($config);

    $pdo = $this->createPdo($dsn, $config['username'], $config['password'], $config['params']);

    $this->pdo = $pdo;

    $this->tableName = $config['prefix'] . 'debug_log';
  }

  public function save(array $log): bool
  {


    $app_name = app('http')->getName() ?: '';

    $controller_name = request()->controller();
    $action_name = request()->action();

    if (App::runningInConsole()) {
      $app_name = 'cli';
    }

    $create_time = time();


    $create_time_title = date('Y-m-d H:i:s', $create_time);

    $log_key = uniqid();

    foreach ($log as $log_level => $log_list) {
      foreach ($log_list as $key => $log_item) {

        if (!is_string($log_item)) {
          $log_item = json_encode($log_item, JSON_UNESCAPED_UNICODE);
        }

        $log_data = [
          'level' => $log_level,
          'content' => $log_item,
          'create_time' => $create_time,
          'create_time_title' => $create_time_title,
          'uid' => $log_key,
          'app_name' => $app_name,
          'controller_name' => $controller_name,
          'action_name' => $action_name,
        ];

        foreach ($log_data as $key => &$value) {
          $value = str_replace('\'', '\\\'', $value);
        }

        $data_keys = array_keys($log_data);

        $data_keys_in_sql = join(',', $data_keys);

        $data_values_in_sql = join('\',\'', $log_data);

        $sql = "INSERT INTO {$this->tableName} ($data_keys_in_sql)  VALUES ('$data_values_in_sql');";

        $this->pdo->exec($sql);
      }
    }

    return true;
  }

  /**
   * 解析pdo连接的dsn信息
   * @access protected
   * @param  array $config 连接信息
   * @return string
   */
  protected function parseDsn(array $config): string
  {
    if (!empty($config['socket'])) {
      $dsn = 'mysql:unix_socket=' . $config['socket'];
    } elseif (!empty($config['hostport'])) {
      $dsn = 'mysql:host=' . $config['hostname'] . ';port=' . $config['hostport'];
    } else {
      $dsn = 'mysql:host=' . $config['hostname'];
    }
    $dsn .= ';dbname=' . $config['database'];

    if (!empty($config['charset'])) {
      $dsn .= ';charset=' . $config['charset'];
    }

    return $dsn;
  }

  protected function createPdo($dsn, $username, $password, $params)
  {
    return new PDO($dsn, $username, $password, $params);
  }

  public function __destruct()
  {
    $this->pdo = null;
  }
}
