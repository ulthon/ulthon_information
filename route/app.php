<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use app\common\tools\Rss;
use app\common\tools\Sitemap;
use app\Request;
use think\facade\Cache;
use think\facade\Log;
use think\facade\Route;

Route::rule('/sitemap.xml', function (Request $request) {

    $cache_key = 'sitemap_last_etag';

    $last_etag = Cache::get($cache_key);

    if (!empty($cache_key)) {

        $if_not_match = $request->header('If-None-Match');

        if (!empty($if_not_match)) {
            if ($if_not_match == $last_etag) {
                return xml('', 304)->eTag($last_etag);
            }
        }
    }

    $content = Sitemap::init();

    $last_etag = md5($content);

    Cache::set($cache_key, $last_etag, 600);

    return xml($content)->eTag($last_etag);
});


Route::rule('/rss1.xml', function (Request $request) {

    $cache_key = 'rss1_cache_key';

    $last_etag = Cache::get($cache_key);

    if (!empty($cache_key)) {

        $if_not_match = $request->header('If-None-Match');

        if (!empty($if_not_match)) {
            if ($if_not_match == $last_etag) {
                return xml('', 304)->eTag($last_etag);
            }
        }
    }

    $content = Rss::initRss1();

    $last_etag = md5($content);

    Cache::set($cache_key, $last_etag, 600);

    return xml($content)->eTag($last_etag);
});

Route::rule('/rss2.xml', function (Request $request) {

    $cache_key = 'rss2_cache_key';

    $last_etag = Cache::get($cache_key);

    if (!empty($cache_key)) {

        $if_not_match = $request->header('If-None-Match');

        if (!empty($if_not_match)) {
            if ($if_not_match == $last_etag) {
                return xml('', 304)->eTag($last_etag);
            }
        }
    }

    $content = Rss::initRss2();

    $last_etag = md5($content);

    Cache::set($cache_key, $last_etag, 600);

    return xml($content)->eTag($last_etag);
});

Route::rule('/atom.xml', function (Request $request) {

    $cache_key = 'atom_cache_key';

    $last_etag = Cache::get($cache_key);

    if (!empty($cache_key)) {

        $if_not_match = $request->header('If-None-Match');

        if (!empty($if_not_match)) {
            if ($if_not_match == $last_etag) {
                return xml('', 304)->eTag($last_etag);
            }
        }
    }

    $content = Rss::initAtom();

    $last_etag = md5($content);

    Cache::set($cache_key, $last_etag, 600);

    return xml($content)->eTag($last_etag);
});