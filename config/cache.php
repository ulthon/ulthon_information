<?php

use think\facade\Env;
use think\facade\Request;

// +----------------------------------------------------------------------
// | 缓存设置
// +----------------------------------------------------------------------

$default = Env::get('cache.driver', 'file');

if (Request::isCli()) {
    $default = 'file';
}

return [
    // 默认缓存驱动
    'default' => $default,

    // 缓存连接方式配置
    'stores' => [
        'file' => [
            // 驱动方式
            'type' => 'File',
            // 缓存保存目录
            'path' => '',
            // 缓存前缀
            'prefix' => '',
            // 缓存有效期 0表示永久缓存
            'expire' => 0,
            // 缓存标签前缀
            'tag_prefix' => 'tag:',
            // 序列化机制 例如 ['serialize', 'unserialize']
            'serialize' => [],
        ],

        // redis缓存
        'redis' => [
            // 驱动方式
            'type' => 'redis',
            // 服务器地址
            'host' => Env::get('cache.redis_host', '127.0.0.1'),
            'select' => Env::get('cache.redis_select', 0),
            'prefix' => '',
            'password' => Env::get('cache.redis_password', ''),
        ],
        // 更多的缓存连接
    ],
];
